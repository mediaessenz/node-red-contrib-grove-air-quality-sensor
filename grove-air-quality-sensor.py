#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# The MIT License (MIT)
#
# Grove Base Hat for the Raspberry Pi, used to connect grove sensors.
# Copyright (C) 2018  Seeed Technology Co.,Ltd.
'''
This is the code for
    - `Grove -  Air Quality Sensor <https://www.seeedstudio.com/Grove-Air-quality-sensor-v1.3-p-2439.html)>`_
'''
import time, sys, io
from grove.adc import ADC

__all__ = ["GroveAirQualitySensor"]

class GroveAirQualitySensor(object):
    '''
    Grove Air Quality Sensor class

    Args:
        pin(int): number of analog pin/channel the sensor connected.
    '''
    def __init__(self, channel):
        self.channel = channel
        self.adc = ADC()

    @property
    def value(self):
        '''
        Get the air quality strength value, badest value is 100.0%.

        Returns:
            (int): ratio, 0(0.0%) - 1000(100.0%)
        '''
        return self.adc.read(self.channel)

Grove = GroveAirQualitySensor


def main():
    from grove.helper import SlotHelper
    save_stdout = sys.stdout
    sys.stdout = io.BytesIO()
    sh = SlotHelper(SlotHelper.ADC)
    sys.stdout = save_stdout
    pin = sh.argv2pin()

    sensor = GroveAirQualitySensor(pin)

    print(sensor.value)

if __name__ == '__main__':
    main()
