This is a Node-RED node to get readings from the Grove Air Quality Sensor(https://wiki.seeedstudio.com/Grove-Air_Quality_Sensor_v1.3/) connected to the Grove Base Hat for Raspberry Pi (https://wiki.seeedstudio.com/Grove_Base_Hat_for_Raspberry_Pi/).

To get it working you have to install it in Node-RED at least ;-)

After this realize the new node "grove-air-quality-sensor" in the group "grove".
Drop it on your flow and add a trigger to its input and connect a debug node afterwards.
Happily watch the actualy calculated values of the sensor in a nice way. 

The rest is up to you!

The included javascript handles the communication between Node-RED and the python script and hands over the msg.payload.

January 2021
